# Schemas
A collection of Json schemas for custom Json structures created by me.

## Currently Includes
* Song Sounds Song
* Manhunt: Fabric configuration file
* Fab-Man package metadata
* Fab-Man configuration file
